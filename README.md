# Quackr
This is the project of Richard Treier, Vincent Mayers, Minh Nhat Nguyen, Dominik Sparer for TU Dortmund's course Web Tech II in SS17.

Quackr is a simple Twitter clone built on Maven, Spring Boot, Hibernate/JPA, Angular 4 and Bootstrap 4.

## Deployment
1. Install Maven (https://maven.apache.org/install.html).
2. Build the project with `mvn clean install -Pprod`.
3. Go into web directory `cd quackr-web`.
4. Deploy the application `mvn spring-boot:run -Pprod`.

## Development
1. Install Maven (https://maven.apache.org/install.html).
2. Install Node and NPM.
3. Build the backend with `mvn clean install -Plocal-dev`.
4. Serve the backend `cd quackr-web & `mvn spring-boot:run -Plocal-dev`
5. Open a new Terminal in the project base dir.
5. Serve the frontend dev server `cd quackr-web & mvn spring-boot:run -Plocal-dev`