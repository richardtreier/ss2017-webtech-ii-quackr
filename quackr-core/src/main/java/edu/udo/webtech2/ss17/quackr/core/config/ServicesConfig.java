package edu.udo.webtech2.ss17.quackr.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"edu.udo.webtech2.ss17.quackr.core"})
public class ServicesConfig {

}
