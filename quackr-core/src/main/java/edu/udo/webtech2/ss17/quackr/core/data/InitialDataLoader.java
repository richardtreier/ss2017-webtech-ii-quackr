package edu.udo.webtech2.ss17.quackr.core.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import edu.udo.webtech2.ss17.quackr.core.dto.UserRegistrationDto;
import edu.udo.webtech2.ss17.quackr.core.services.UserService;

@Component
@Transactional
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {
	@Autowired
	private transient UserService userService;

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		// Create admin user (if not exists)
		UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
		userRegistrationDto.setUsername("admin");
		userRegistrationDto.setPassword("admin");
		userService.createAdminUserIfNotExists(userRegistrationDto);
	}
}
