package edu.udo.webtech2.ss17.quackr.core.dto;

import java.time.ZonedDateTime;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class QuackDto {
	private Long id;
	private String message;

	private UserDto owner;

	private ZonedDateTime created;
}
