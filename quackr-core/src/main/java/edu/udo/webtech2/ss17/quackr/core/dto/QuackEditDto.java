package edu.udo.webtech2.ss17.quackr.core.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class QuackEditDto {
	private String message;
}
