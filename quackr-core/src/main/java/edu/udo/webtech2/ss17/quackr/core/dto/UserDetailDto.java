package edu.udo.webtech2.ss17.quackr.core.dto;

import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class UserDetailDto extends UserDto {
	private List<QuackDto> quacks;
}
