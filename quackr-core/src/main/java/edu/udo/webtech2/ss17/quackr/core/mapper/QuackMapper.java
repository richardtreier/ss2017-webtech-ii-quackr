package edu.udo.webtech2.ss17.quackr.core.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import edu.udo.webtech2.ss17.quackr.core.dto.QuackDto;
import edu.udo.webtech2.ss17.quackr.core.dto.QuackEditDto;
import edu.udo.webtech2.ss17.quackr.dao.entities.Quack;

@Mapper(uses = { UserMapper.class })
public interface QuackMapper {
	QuackDto quackToQuackDto(Quack quack);

	List<QuackDto> quackToQuackDto(List<Quack> quacks);

	@Mappings({
		@Mapping(target="id", ignore = true),
		@Mapping(target="created", ignore = true),
		@Mapping(target="owner", ignore = true),
	})
	void updateQuackWithQuackEditDto(QuackEditDto quackEditDto, @MappingTarget Quack quack);
}
