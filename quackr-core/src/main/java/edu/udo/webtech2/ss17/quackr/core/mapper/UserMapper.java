package edu.udo.webtech2.ss17.quackr.core.mapper;

import java.util.List;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import edu.udo.webtech2.ss17.quackr.core.dto.UserDetailDto;
import edu.udo.webtech2.ss17.quackr.core.dto.UserDto;
import edu.udo.webtech2.ss17.quackr.core.dto.UserRegistrationDto;
import edu.udo.webtech2.ss17.quackr.dao.entities.User;

@Mapper(uses = QuackMapper.class)
@DecoratedWith(UserMapperDecorator.class)
public interface UserMapper {
	UserDto userToUserDto(User user);

	List<UserDto> userToUserDto(List<User> user);

	@Mappings({ @Mapping(target = "password", ignore = true), @Mapping(target = "id", ignore = true),
			@Mapping(target = "quacks", ignore = true), @Mapping(target = "roles", ignore = true), })
	void updateUserWithUserRegistrationDto(UserRegistrationDto userRegistrationDto, @MappingTarget User user);

	@QuackrDtoDetailMapping
	UserDetailDto userToUserDetailDto(User user);
}
