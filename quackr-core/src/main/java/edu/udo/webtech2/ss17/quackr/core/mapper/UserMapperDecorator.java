package edu.udo.webtech2.ss17.quackr.core.mapper;

import java.util.Collections;
import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import edu.udo.webtech2.ss17.quackr.core.dto.QuackDto;
import edu.udo.webtech2.ss17.quackr.core.dto.UserDetailDto;
import edu.udo.webtech2.ss17.quackr.dao.entities.User;

/**
 * Decorator for UserDetailDtos with created desc ordered UserDetailDto.quacks
 * @author Richard
 *
 */
public abstract class UserMapperDecorator implements UserMapper {
	@Autowired
    @Qualifier("delegate")
	private transient UserMapper delegate;

	/**
	 * Compare Quacks by their creation dates
	 * @author Richard
	 *
	 */
	private class QuackDtoByCreatedComparator implements Comparator<QuackDto> {
		@Override
		public int compare(QuackDto o1, QuackDto o2) {
			return o1.getCreated().compareTo(o2.getCreated());
		}
	}
	
	/**
	 * Add sorting to userToUserDetailDto
	 */
	@Override
	public UserDetailDto userToUserDetailDto(User user) {
		UserDetailDto userDetailDto = delegate.userToUserDetailDto(user);

		if (userDetailDto.getQuacks() != null) {
			Collections.sort(userDetailDto.getQuacks(), new QuackDtoByCreatedComparator().reversed());
		}

		return userDetailDto;
	}
}
