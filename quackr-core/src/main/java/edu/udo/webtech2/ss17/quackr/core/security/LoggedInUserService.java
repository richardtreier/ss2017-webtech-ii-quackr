package edu.udo.webtech2.ss17.quackr.core.security;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;

public interface LoggedInUserService {
	User getLoggedInUser();
	
	boolean isLoggedIn();
	
	LoginStatusDto getLoginStatus();
}
