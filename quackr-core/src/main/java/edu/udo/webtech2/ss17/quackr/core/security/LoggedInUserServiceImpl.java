package edu.udo.webtech2.ss17.quackr.core.security;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.udo.webtech2.ss17.quackr.core.mapper.UserMapper;
import edu.udo.webtech2.ss17.quackr.dao.entities.User;

@Service
public class LoggedInUserServiceImpl implements LoggedInUserService {
	@Autowired
	private transient UserMapper userMapper;
	@Autowired
	private transient UserAuthDataService userAuthDataService;

	@Override
	@Transactional(readOnly = true)
	public User getLoggedInUser() {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			Object principal = currentUser.getPrincipal();
			
			if(principal instanceof Long) {
				return userAuthDataService.findOneById((Long) principal);
			}
			
			if(principal instanceof User) {
				return (User) principal;
			}
			
			throw new RuntimeException("What the fuck is this principal("+principal.getClass().getName().toString()+"): "+principal.toString());
		}

		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean isLoggedIn() {
		return getLoggedInUser() != null;
	}

	@Override
	@Transactional(readOnly = true)
	public LoginStatusDto getLoginStatus() {
		User loggedInUser = getLoggedInUser();

		LoginStatusDto loginStatusDto = new LoginStatusDto();
		if (loggedInUser != null) {
			loginStatusDto.setAuthenticated(true);
			loginStatusDto.setAuthUser(userMapper.userToUserDto(loggedInUser));
			loginStatusDto.setAuthRoles(
					loggedInUser.getRoles().stream().map(role -> role.getName()).collect(Collectors.toList()));
			loginStatusDto.setAuthPermissions(userAuthDataService.getUserUserPermissions(loggedInUser).stream()
					.map(permission -> permission.getName()).collect(Collectors.toList()));
		} else {
			loginStatusDto.setAuthenticated(false);
			loginStatusDto.setAuthUser(null);
			loginStatusDto.setAuthRoles(Arrays.asList());
			loginStatusDto.setAuthPermissions(Arrays.asList());
		}

		return loginStatusDto;
	}
}
