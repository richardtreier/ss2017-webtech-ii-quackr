package edu.udo.webtech2.ss17.quackr.core.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class LoginAttemptDto {
	private String username;
	private String password;
}
