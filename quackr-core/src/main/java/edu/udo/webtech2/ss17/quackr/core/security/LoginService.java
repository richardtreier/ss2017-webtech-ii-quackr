package edu.udo.webtech2.ss17.quackr.core.security;

import org.apache.shiro.authc.AuthenticationException;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;

public interface LoginService {
	void login(LoginAttemptDto loginAttemptDto) throws AuthenticationException;

	void logout();
}
