package edu.udo.webtech2.ss17.quackr.core.security;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoginServiceImpl implements LoginService {
	@Override
	@Transactional(readOnly = true)
	public void login(LoginAttemptDto loginAttemptDto) throws AuthenticationException {
		UsernamePasswordToken token = new UsernamePasswordToken(loginAttemptDto.getUsername(),
				loginAttemptDto.getPassword());
		token.setRememberMe(true);
		SecurityUtils.getSubject().login(token); // Throws
													// AuthenticationException
	}

	@Override
	@Transactional(readOnly = true)
	public void logout() {
		SecurityUtils.getSubject().logout();
	}

}
