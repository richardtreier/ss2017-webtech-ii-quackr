package edu.udo.webtech2.ss17.quackr.core.security;

import java.util.ArrayList;
import java.util.List;

import edu.udo.webtech2.ss17.quackr.core.dto.UserDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class LoginStatusDto {
	
	private boolean authenticated;
	
	private UserDto authUser;
	
	private List<String> authRoles;
	
	private List<String> authPermissions;
}
