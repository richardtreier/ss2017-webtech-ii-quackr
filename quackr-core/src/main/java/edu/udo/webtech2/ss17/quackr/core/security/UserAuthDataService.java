package edu.udo.webtech2.ss17.quackr.core.security;

import java.util.List;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.entities.UserPermission;

public interface UserAuthDataService {
	List<UserPermission> getUserUserPermissions(User user);

	User findOneByUsername(String username);

	User findOneById(Long userId);
}
