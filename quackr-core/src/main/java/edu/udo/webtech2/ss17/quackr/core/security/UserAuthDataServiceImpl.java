package edu.udo.webtech2.ss17.quackr.core.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.entities.UserPermission;
import edu.udo.webtech2.ss17.quackr.dao.services.UserModelService;
import edu.udo.webtech2.ss17.quackr.dao.services.UserPermissionModelService;

@Service
public class UserAuthDataServiceImpl implements UserAuthDataService {
	@Autowired
	private transient UserPermissionModelService userPermissionModelService;
	@Autowired
	private transient UserModelService userModelService;
	
	@Transactional(readOnly = true)
	@Override
 	public List<UserPermission> getUserUserPermissions(User user) {
		return userPermissionModelService.findByUser(user);
	}

	@Transactional(readOnly = true)
	@Override
	public User findOneByUsername(String username) {
		return userModelService.findOneByUsername(username);
	}
	@Transactional(readOnly = true)
	@Override
	public User findOneById(Long userId) {
		return userModelService.findOne(userId);
	}
}
