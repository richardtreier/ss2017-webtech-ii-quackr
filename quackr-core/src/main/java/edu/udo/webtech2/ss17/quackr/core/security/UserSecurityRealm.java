package edu.udo.webtech2.ss17.quackr.core.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;

/**
 * Security Realm to connect to our Service Layer
 * 
 * @author Richard
 *
 */
public class UserSecurityRealm extends AuthorizingRealm {

	@Autowired
	private UserAuthDataService userAuthDataService;

	/**
	 * User(s) -> Roles + Permissions
	 */
	@Override
	@Transactional(readOnly = true)
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
		Set<String> roles = new HashSet<>();
		Set<Permission> permissions = new HashSet<>();

		Collection<Long> userIds = principalCollection.byType(Long.class);
		for (Long userId : userIds) {
			User user = userAuthDataService.findOneById(userId);
			
			// Add roles
			roles.addAll(user.getRoles().stream().map(role -> role.getName()).collect(Collectors.toList()));

			// Add permissions
			permissions.addAll(userAuthDataService.getUserUserPermissions(user).stream()
					.map(permission -> new WildcardPermission(permission.getName())).collect(Collectors.toList()));
		}

		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
		info.setRoles(roles);
		info.setObjectPermissions(permissions);
		return info;
	}

	/**
	 * AuthenticationToken -> User
	 */
	@Override
	@Transactional(readOnly = true)
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
			throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
		User user = userAuthDataService.findOneByUsername(token.getUsername());

		if (user != null) {
			return new SimpleAuthenticationInfo(user.getId(), user.getPassword(), this.getName());
		}

		return null;
	}
}