package edu.udo.webtech2.ss17.quackr.core.services;

import java.util.List;

import edu.udo.webtech2.ss17.quackr.core.dto.QuackDto;
import edu.udo.webtech2.ss17.quackr.core.dto.QuackEditDto;

public interface QuackService {
	List<QuackDto> findAllOrderByCreatedDesc();
	QuackDto findOne(Long id);
	
	QuackDto create(QuackEditDto quackEditDto);
	QuackDto edit(Long id, QuackEditDto quackEditDto);
	void delete(Long id);
} 
