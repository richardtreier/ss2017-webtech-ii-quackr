package edu.udo.webtech2.ss17.quackr.core.services;

import java.time.ZonedDateTime;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.udo.webtech2.ss17.quackr.core.dto.QuackDto;
import edu.udo.webtech2.ss17.quackr.core.dto.QuackEditDto;
import edu.udo.webtech2.ss17.quackr.core.mapper.QuackMapper;
import edu.udo.webtech2.ss17.quackr.core.security.LoggedInUserService;
import edu.udo.webtech2.ss17.quackr.dao.entities.Quack;
import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.services.QuackModelService;

@Service
public class QuackServiceImpl implements QuackService {
	@Autowired
	private transient QuackModelService quackModelService;

	@Autowired
	private transient QuackMapper quackMapper;
	
	@Autowired
	private transient LoggedInUserService loggedInUserService;

	@Override
	@Transactional(readOnly = true)
	public List<QuackDto> findAllOrderByCreatedDesc() {
		return quackMapper.quackToQuackDto(quackModelService.findAllOrderByCreatedDesc());
	}

	@Override
	@Transactional(readOnly = true)
	public QuackDto findOne(Long id) {
		Quack quack = quackModelService.findOne(id);
		if(quack == null) {
			throw new ResourceDoesNotExistException("Quack does not exist.");
		}
		
		return quackMapper.quackToQuackDto(quack);
	}

	@Override
	@Transactional
	@RequiresAuthentication
	public QuackDto create(QuackEditDto quackEditDto) {
		User loggedInUser = loggedInUserService.getLoggedInUser();
		if(loggedInUser == null) {
			throw new RuntimeException("User cannot be null, how are you authenticated??");
		}
		
		Quack quack = new Quack();
		quack.setCreated(ZonedDateTime.now());
		quack.setOwner(loggedInUser);

		quackMapper.updateQuackWithQuackEditDto(quackEditDto, quack);
		quack = quackModelService.save(quack);

		return quackMapper.quackToQuackDto(quack);
	}

	@Override
	@Transactional
	@RequiresAuthentication
	public QuackDto edit(Long id, QuackEditDto quackEditDto) {
		Quack quack = quackModelService.findOne(id);
		if(quack == null) {
			throw new ResourceDoesNotExistException("Quack does not exist.");
		}
		
		User loggedInUser = loggedInUserService.getLoggedInUser();
		if(loggedInUser == null) {
			throw new RuntimeException("User cannot be null, how are you authenticated??");
		}
		
		if(quack.getOwner() != loggedInUser && !SecurityUtils.getSubject().hasRole("ADMIN")) {
			throw new org.apache.shiro.authz.AuthorizationException("You don't have the permission to edit this quack.");
		}

		quackMapper.updateQuackWithQuackEditDto(quackEditDto, quack);
		quack = quackModelService.save(quack);

		return quackMapper.quackToQuackDto(quack);
	}

	@Override
	@Transactional
	@RequiresAuthentication
	public void delete(Long id) {
		Quack quack = quackModelService.findOne(id);
		if(quack == null) {
			throw new ResourceDoesNotExistException("Quack does not exist.");
		}
		
		User loggedInUser = loggedInUserService.getLoggedInUser();
		if(loggedInUser == null) {
			throw new RuntimeException("User cannot be null, how are you authenticated??");
		}
		
		if(quack.getOwner() != loggedInUser && !SecurityUtils.getSubject().hasRole("ADMIN")) {
			throw new org.apache.shiro.authz.AuthorizationException("You don't have the permission to delete this quack.");
		}

		quackModelService.delete(quack);
	}

}
