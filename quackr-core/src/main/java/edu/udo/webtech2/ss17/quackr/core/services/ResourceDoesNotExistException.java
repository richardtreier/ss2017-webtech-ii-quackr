package edu.udo.webtech2.ss17.quackr.core.services;

public class ResourceDoesNotExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8434167971225755917L;

	public ResourceDoesNotExistException(String message) {
		super(message);
	}
}
