package edu.udo.webtech2.ss17.quackr.core.services;

public class UserAlreadyExistsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8434167971225755917L;

	public UserAlreadyExistsException(String message) {
		super(message);
	}
}
