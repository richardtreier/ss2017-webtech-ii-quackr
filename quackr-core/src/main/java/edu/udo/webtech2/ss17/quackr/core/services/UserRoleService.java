package edu.udo.webtech2.ss17.quackr.core.services;

import edu.udo.webtech2.ss17.quackr.dao.entities.UserRole;

public interface UserRoleService {
	UserRole findOrCreateRoleByName(String name);
}
