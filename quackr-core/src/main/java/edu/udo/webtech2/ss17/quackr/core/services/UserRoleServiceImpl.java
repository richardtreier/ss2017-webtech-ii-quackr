package edu.udo.webtech2.ss17.quackr.core.services;

import java.util.LinkedHashSet;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.entities.UserPermission;
import edu.udo.webtech2.ss17.quackr.dao.entities.UserRole;
import edu.udo.webtech2.ss17.quackr.dao.services.UserRoleModelService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private transient UserRoleModelService userRoleModelService;

	@Override
	@Transactional
	public UserRole findOrCreateRoleByName(String name) {
		UserRole userRole = userRoleModelService.findOneByName(name);
		if(userRole == null) {
			userRole = new UserRole();
			userRole.setName(name);
			userRole.setPermissions(new LinkedHashSet<UserPermission>());
			userRole.setUsers(new LinkedHashSet<User>());
			userRole = userRoleModelService.save(userRole);
		}
		
		return userRole;
	}

}
