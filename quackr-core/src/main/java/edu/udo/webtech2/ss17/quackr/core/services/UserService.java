package edu.udo.webtech2.ss17.quackr.core.services;

import java.util.List;

import edu.udo.webtech2.ss17.quackr.core.dto.UserDetailDto;
import edu.udo.webtech2.ss17.quackr.core.dto.UserDto;
import edu.udo.webtech2.ss17.quackr.core.dto.UserRegistrationDto;
import edu.udo.webtech2.ss17.quackr.dao.entities.User;

public interface UserService {
	UserDto createUser(UserRegistrationDto userRegistrationDto);

	UserDto createUserAndLogin(UserRegistrationDto userRegistrationDto);

	UserDto findOneByUsername(String username);

	List<UserDto> findAllOrderByUsernameAsc();

	UserDetailDto findOneByUsernameDetail(String username);

	User createAdminUserIfNotExists(UserRegistrationDto userRegistrationDto);
}
