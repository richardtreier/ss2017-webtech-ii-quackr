package edu.udo.webtech2.ss17.quackr.core.services;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.udo.webtech2.ss17.quackr.core.dto.UserDetailDto;
import edu.udo.webtech2.ss17.quackr.core.dto.UserDto;
import edu.udo.webtech2.ss17.quackr.core.dto.UserRegistrationDto;
import edu.udo.webtech2.ss17.quackr.core.mapper.UserMapper;
import edu.udo.webtech2.ss17.quackr.core.security.LoginAttemptDto;
import edu.udo.webtech2.ss17.quackr.core.security.LoginService;
import edu.udo.webtech2.ss17.quackr.dao.entities.Quack;
import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.entities.UserRole;
import edu.udo.webtech2.ss17.quackr.dao.services.UserModelService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private transient PasswordService passwordService;

	@Autowired
	private transient UserMapper userMapper;

	@Autowired
	private transient UserModelService userModelService;

	@Autowired
	private transient UserRoleService userRoleService;

	@Autowired
	private transient LoginService loginService;

	@Override
	@Transactional
	public User createAdminUserIfNotExists(UserRegistrationDto userRegistrationDto) {
		// Create Admin Role if not exists
		UserRole userRoleAdmin = userRoleService.findOrCreateRoleByName("ADMIN");

		// Create Admin user account if not exists
		User userAdmin = userModelService.findOneByUsername(userRegistrationDto.getUsername());
		if (userAdmin == null) {
			createUser(userRegistrationDto);
			userAdmin = userModelService.findOneByUsername(userRegistrationDto.getUsername()); 
		}
		
		//Set Admin user role
		Set<UserRole> userRoles = new LinkedHashSet<UserRole>();
		userRoles.add(userRoleAdmin);
		userAdmin.setRoles(userRoles);
		userAdmin = userModelService.save(userAdmin);
		
		return userAdmin;
	}
	
	

	@Override
	@Transactional
	public UserDto createUser(UserRegistrationDto userRegistrationDto) {
		// Does the user exist already?
		User existingUser = this.userModelService.findOneByUsername(userRegistrationDto.getUsername());
		if (existingUser != null) {
			throw new UserAlreadyExistsException("Username already taken.");
		}

		// Create user
		User user = new User();
		user.setQuacks(new LinkedHashSet<Quack>());
		user.setRoles(new LinkedHashSet<UserRole>());

		userMapper.updateUserWithUserRegistrationDto(userRegistrationDto, user);

		user.setPassword(passwordService.encryptPassword(userRegistrationDto.getPassword()));
		user = userModelService.save(user);

		return userMapper.userToUserDto(user);
	}

	@Override
	@Transactional
	public UserDto createUserAndLogin(UserRegistrationDto userRegistrationDto) {
		UserDto user = createUser(userRegistrationDto);
		
		LoginAttemptDto loginAttemptDto = new LoginAttemptDto();
		loginAttemptDto.setUsername(userRegistrationDto.getUsername());
		loginAttemptDto.setPassword(userRegistrationDto.getPassword());
			
		this.loginService.login(loginAttemptDto);
		return user;
	}

	@Override
	@Transactional(readOnly = true)
	public UserDto findOneByUsername(String username) {
		return userMapper.userToUserDto(userModelService.findOneByUsername(username));
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetailDto findOneByUsernameDetail(String username) {
		return userMapper.userToUserDetailDto(userModelService.findOneByUsername(username));
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserDto> findAllOrderByUsernameAsc() {
		return userMapper.userToUserDto(userModelService.findAllOrderByUsernameAsc());
	}

}
