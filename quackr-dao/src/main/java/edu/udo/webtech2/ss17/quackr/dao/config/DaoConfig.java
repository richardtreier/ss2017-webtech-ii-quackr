package edu.udo.webtech2.ss17.quackr.dao.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import edu.udo.webtech2.ss17.quackr.dao.db.QuackrImplicitNamingStrategy;
import edu.udo.webtech2.ss17.quackr.dao.db.QuackrPhysicalNamingStrategy;

@Configuration
@ComponentScan({ "edu.udo.webtech2.ss17.quackr.dao" })
@PropertySource({ "classpath:dao.properties", "classpath:hibernate.properties"})
@EnableJpaRepositories("edu.udo.webtech2.ss17.quackr.dao.repositories")
@EnableTransactionManagement
public class DaoConfig {
	public static final String[] ENTITY_PKGS = new String[] { "edu.udo.webtech2.ss17.quackr.dao.entities" };

	@Autowired
	protected transient Environment env;
	
	@Autowired
	private transient DataSource dataSource;
	
	@Bean
	public EntityManagerFactory entityManagerFactory() {
		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(true);
		vendorAdapter.setShowSql(env.getProperty("hibernate.show_sql", Boolean.class));

		final LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setPackagesToScan(ENTITY_PKGS);
		factory.setDataSource(dataSource);

		Properties jpaProperties = new Properties();
		jpaProperties.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		jpaProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));

		// Set up naming strategies
		jpaProperties.put("hibernate.physical_naming_strategy", QuackrPhysicalNamingStrategy.class.getName());
		jpaProperties.put("hibernate.implicit_naming_strategy", QuackrImplicitNamingStrategy.class.getName());

		factory.setJpaProperties(jpaProperties);
		factory.afterPropertiesSet();

		return factory.getObject();
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		final JpaTransactionManager tm = new JpaTransactionManager();
		tm.setEntityManagerFactory(entityManagerFactory());
		return tm;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
