package edu.udo.webtech2.ss17.quackr.dao.db;

import java.util.Iterator;
import java.util.Locale;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitIdentifierColumnNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.model.naming.ImplicitUniqueKeyNameSource;

public class QuackrImplicitNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl {

    public static final QuackrImplicitNamingStrategy INSTANCE = new QuackrImplicitNamingStrategy();
    
    public static final Integer IDENTIFIER_MAX_LENGTH = 64;

    public QuackrImplicitNamingStrategy() {
    }
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Override
    public Identifier determineIdentifierColumnName(ImplicitIdentifierColumnNameSource source) {
        // JPA states the implicit column name should be the attribute name
        return toIdentifier(
                transformAttributePath( source.getIdentifierAttributePath() ).toUpperCase(Locale.ROOT),
                source.getBuildingContext()
        );
    }
    
    @Override
    public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {
        StringBuilder sb = new StringBuilder();
        sb.append("FK_")
            .append(source.getTableName().getText()).append("_")
            .append(source.getReferencedTableName().getText());
        Iterator<Identifier> it = source.getColumnNames().iterator();
        
        while (it.hasNext()) {
            sb.append("_").append(it.next().getText());
        }
        if(sb.length() > IDENTIFIER_MAX_LENGTH){
            return super.determineForeignKeyName(source);
        }
        return toIdentifier(sb.toString(), source.getBuildingContext());
    }

    @Override
    public Identifier determineUniqueKeyName(ImplicitUniqueKeyNameSource source) {
        StringBuilder sb = new StringBuilder();
        sb.append("UK_").append(source.getTableName().getText());
        Iterator<Identifier> it = source.getColumnNames().iterator();
        while (it.hasNext()) {
            sb.append("_").append(it.next().getText());
        }
        if(sb.length() > IDENTIFIER_MAX_LENGTH){
            return super.determineUniqueKeyName(source);
        }
        return toIdentifier(
                sb.toString(),
                source.getBuildingContext()
        );
    }

}
