package edu.udo.webtech2.ss17.quackr.dao.entities;

import javax.persistence.Entity;

import org.hibernate.envers.Audited;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Audited
@Setter
@Getter
@RequiredArgsConstructor
public class FirstTestEntityPerson extends QuackrAbstractBaseEntity {
	private String firstName;
	private String lastName;
}
