package edu.udo.webtech2.ss17.quackr.dao.entities;

import java.time.ZonedDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Quack extends QuackrAbstractBaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7753787381321262548L;

	private String message;
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	private User owner;
	
	private ZonedDateTime created;
}
