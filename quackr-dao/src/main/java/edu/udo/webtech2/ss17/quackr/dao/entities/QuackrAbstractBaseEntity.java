package edu.udo.webtech2.ss17.quackr.dao.entities;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

import org.hibernate.envers.Audited;

@lombok.Getter
@lombok.Setter
@lombok.ToString
@lombok.NoArgsConstructor	
@MappedSuperclass
@Audited
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class QuackrAbstractBaseEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1977629312663785678L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
}
