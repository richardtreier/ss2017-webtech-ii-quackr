package edu.udo.webtech2.ss17.quackr.dao.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class User extends QuackrAbstractBaseEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -400472226435625168L;

	@Column(unique = true, nullable = false)
	private String username;

	private String password;

	@ManyToMany
	private Set<UserRole> roles;
	
	@OneToMany(mappedBy="owner")
	private Set<Quack> quacks;
}
