package edu.udo.webtech2.ss17.quackr.dao.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class UserPermission extends QuackrAbstractBaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1623915720922526924L;

	private String name;
	
	@ManyToMany(mappedBy="permissions")
	private Set<UserRole> roles;
}
