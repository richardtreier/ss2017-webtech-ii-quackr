package edu.udo.webtech2.ss17.quackr.dao.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class UserRole extends QuackrAbstractBaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3232184203957915922L;

	private String name;

	@ManyToMany(mappedBy = "roles")
	private Set<User> users;

	@ManyToMany
	private Set<UserPermission> permissions;
}
