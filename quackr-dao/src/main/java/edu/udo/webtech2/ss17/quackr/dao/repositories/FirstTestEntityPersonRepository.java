package edu.udo.webtech2.ss17.quackr.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.udo.webtech2.ss17.quackr.dao.entities.FirstTestEntityPerson;

@Repository
public interface FirstTestEntityPersonRepository extends JpaRepository<FirstTestEntityPerson, Long> {
	public List<FirstTestEntityPerson> findAll();
	public FirstTestEntityPerson findByFirstName(String name);
}
