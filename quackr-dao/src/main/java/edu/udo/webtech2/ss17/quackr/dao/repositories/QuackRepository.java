package edu.udo.webtech2.ss17.quackr.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.udo.webtech2.ss17.quackr.dao.entities.Quack;
import edu.udo.webtech2.ss17.quackr.dao.entities.User;

@Repository 
public interface QuackRepository extends JpaRepository<Quack, Long> {
	List<Quack> findByOrderByCreatedDesc();
	List<Quack> findAllByOwnerOrderByCreatedDesc(User owner);
}
