package edu.udo.webtech2.ss17.quackr.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.entities.UserPermission;

@Repository 
public interface UserPermissionRepository extends JpaRepository<UserPermission, Long> {

	List<UserPermission> findAllByRoles_Users(User user);

}
