package edu.udo.webtech2.ss17.quackr.dao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;

@Repository 
public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsername(String username);

	List<User> findAllByOrderByUsernameAsc();

}
