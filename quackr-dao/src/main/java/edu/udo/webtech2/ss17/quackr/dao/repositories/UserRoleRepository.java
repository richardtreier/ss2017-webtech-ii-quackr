package edu.udo.webtech2.ss17.quackr.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.udo.webtech2.ss17.quackr.dao.entities.UserRole;

@Repository 
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

	UserRole findOneByName(String name);

}
