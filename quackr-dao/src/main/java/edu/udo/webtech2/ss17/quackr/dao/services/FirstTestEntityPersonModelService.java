package edu.udo.webtech2.ss17.quackr.dao.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import edu.udo.webtech2.ss17.quackr.dao.entities.FirstTestEntityPerson;
import edu.udo.webtech2.ss17.quackr.dao.repositories.FirstTestEntityPersonRepository;


@Service
public class FirstTestEntityPersonModelService extends QuackrAbstractBaseModelService<FirstTestEntityPerson, Long> {
	
	@Autowired
	private transient FirstTestEntityPersonRepository personRepository;

	/**
	 * 
	 */
	@Override
	public JpaRepository<FirstTestEntityPerson, Long> getRepository() {
		return personRepository;
	}

}
