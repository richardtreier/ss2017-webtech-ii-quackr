package edu.udo.webtech2.ss17.quackr.dao.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import edu.udo.webtech2.ss17.quackr.dao.entities.Quack;
import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.repositories.QuackRepository;

@Service
public class QuackModelService extends QuackrAbstractBaseModelService<Quack, Long> {
	@Autowired
	private transient QuackRepository quackRepository;

	@Override
	public JpaRepository<Quack, Long> getRepository() {
		return quackRepository;
	}
	
	public List<Quack> findAllOrderByCreatedDesc() {
		return this.quackRepository.findByOrderByCreatedDesc();
	}
	
	public List<Quack> findByOwnerOrderByCreatedDesc(User owner) {
		return this.quackRepository.findAllByOwnerOrderByCreatedDesc(owner);
	}

}
