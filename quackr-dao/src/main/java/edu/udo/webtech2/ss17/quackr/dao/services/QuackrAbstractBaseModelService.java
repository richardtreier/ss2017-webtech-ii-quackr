package edu.udo.webtech2.ss17.quackr.dao.services;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import edu.udo.webtech2.ss17.quackr.dao.entities.QuackrAbstractBaseEntity;

/**
 * Base Model Service
 * @author Richard
 *
 * @param <ENTITY>
 * @param <PKEY>
 */
public abstract class QuackrAbstractBaseModelService<ENTITY extends QuackrAbstractBaseEntity, PKEY extends Serializable> {
	public abstract JpaRepository<ENTITY, PKEY> getRepository();


	@Transactional(readOnly = true)
	public ENTITY findOne(final PKEY key) {
		return getRepository().findOne(key);
	}

	@Transactional(readOnly = true)
	public List<ENTITY> findAll() {
		return getRepository().findAll();
	}

	@Transactional
	public ENTITY save(final ENTITY entity) {
		return getRepository().saveAndFlush(entity);
	}

	@Transactional
	public List<ENTITY> save(final List<ENTITY> entities) {
		List<ENTITY> result = entities.stream().map(e -> getRepository().save(e)).collect(Collectors.toList());
		getRepository().flush();
		return result;
	}

	@Transactional
	public void delete(final ENTITY entity) {
		getRepository().delete(entity);
	}

	@Transactional
	public void delete(final PKEY pkey) {
		getRepository().delete(pkey);
	}
}
