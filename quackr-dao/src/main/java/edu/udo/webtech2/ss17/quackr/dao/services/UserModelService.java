package edu.udo.webtech2.ss17.quackr.dao.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.repositories.UserRepository;

@Service
public class UserModelService extends QuackrAbstractBaseModelService<User, Long> {
	@Autowired
	private transient UserRepository userRepository;

	@Override
	public JpaRepository<User, Long> getRepository() {
		return userRepository;
	}

	public User findOneByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public List<User> findAllOrderByUsernameAsc() {
		return userRepository.findAllByOrderByUsernameAsc();
	}
}
