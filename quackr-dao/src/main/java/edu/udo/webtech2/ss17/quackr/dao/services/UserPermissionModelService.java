package edu.udo.webtech2.ss17.quackr.dao.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import edu.udo.webtech2.ss17.quackr.dao.entities.User;
import edu.udo.webtech2.ss17.quackr.dao.entities.UserPermission;
import edu.udo.webtech2.ss17.quackr.dao.repositories.UserPermissionRepository;

@Service
public class UserPermissionModelService extends QuackrAbstractBaseModelService<UserPermission, Long> {
	@Autowired
	private transient UserPermissionRepository userPermissionRepository;

	@Override
	public JpaRepository<UserPermission, Long> getRepository() {
		return userPermissionRepository;
	}

	public List<UserPermission> findByUser(User user) {
		return userPermissionRepository.findAllByRoles_Users(user);
	}
}
