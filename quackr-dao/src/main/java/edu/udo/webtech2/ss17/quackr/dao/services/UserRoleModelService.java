package edu.udo.webtech2.ss17.quackr.dao.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import edu.udo.webtech2.ss17.quackr.dao.entities.UserRole;
import edu.udo.webtech2.ss17.quackr.dao.repositories.UserRoleRepository;

@Service
public class UserRoleModelService extends QuackrAbstractBaseModelService<UserRole, Long> {
	@Autowired
	private transient UserRoleRepository userRoleRepository;

	@Override
	public JpaRepository<UserRole, Long> getRepository() {
		return userRoleRepository;
	}

	public UserRole findOneByName(String name) {
		return userRoleRepository.findOneByName(name);
	}
}
