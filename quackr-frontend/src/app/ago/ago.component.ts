import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-ago',
  templateUrl: './ago.component.html',
  styleUrls: ['./ago.component.css']
})
export class AgoComponent implements OnInit {
  @Input()
  public date: Date;

  public message: string;

  private subscription: Subscription;

  constructor() { }

  ngOnInit() {
    this.subscription = Observable.interval(1000).startWith(0).map(() => {
      var result: string;
      // current time
      let now = new Date().getTime();

      // time since message was sent in seconds
      let delta = (now - new Date(this.date).getTime()) / 1000;

      // format string
      if (delta < 10) {
        result = 'now';
      }
      else if (delta < 60) { // sent in last minute
        result = '' + Math.floor(delta) + ' seconds ago';
      }
      else if (delta < 3600) { // sent in last hour
        result = '' + Math.floor(delta / 60) + ' minutes ago';
      }
      else if (delta < 86400) { // sent on last day
        result = '' + Math.floor(delta / 3600) + ' hours ago';
      }
      else { // sent more than one day ago
        result = '' + Math.floor(delta / 86400) + ' days ago';
      }
      return result;
    }).subscribe((message: string) => {
      this.message = message;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
