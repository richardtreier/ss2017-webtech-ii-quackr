import { LoginService } from './services/login.service';
import { UserService } from './services/user.service';
import { QuackService } from './services/quack.service';
import { IndexService } from './services/index.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { LandingComponent } from './landing/landing.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { QuacksListThreeComponent } from './quacks-list-three/quacks-list-three.component';
import { AgoComponent } from './ago/ago.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { QuackComponent } from './quack/quack.component';
import { AutoFocusAndSelectDirective } from './auto-focus-and-select.directive';
import { QuackCreateComponent } from './quack-create/quack-create.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    LandingComponent,
    PageNotFoundComponent,
    LoginComponent,
    RegisterComponent,
    QuacksListThreeComponent,
    AgoComponent,
    UserDetailComponent,
    QuackComponent,
    AutoFocusAndSelectDirective,
    QuackCreateComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, 
    AppRoutingModule
  ],
  providers: [
    IndexService,
    QuackService,
    UserService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
