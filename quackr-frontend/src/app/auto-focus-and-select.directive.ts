import { Directive, ElementRef, Renderer, Input } from '@angular/core';

@Directive({
    selector: '[autoFocusAndSelect]'
})
export class AutoFocusAndSelectDirective
{
    private _autoFocusAndSelect;
    constructor(private el: ElementRef, private renderer: Renderer)
    {
    }

    ngOnInit()
    {
    }

    ngAfterViewInit()
    {
        if (this._autoFocusAndSelect || typeof this._autoFocusAndSelect === "undefined")
            this.renderer.invokeElementMethod(this.el.nativeElement, 'focus', []);
            this.renderer.invokeElementMethod(this.el.nativeElement, 'select', []);
    }

    @Input() set autoFocusAndSelect(condition: boolean)
    {
        this._autoFocusAndSelect = condition != false;
    }
}