import { Title } from '@angular/platform-browser';
import { IndexService } from '../services/index.service';
import { UserDto } from '../services/user.service';
import { Subscription } from 'rxjs/Subscription';
import { QuackDto, QuackEditDto, QuackService } from './../services/quack.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  loggedInUser: UserDto;
  quacks: QuackDto[];
  quacksSubscription: Subscription;
  loggedInUserSubscription: Subscription;

  constructor(
    private router: Router,
    private quackService: QuackService,
    private indexService: IndexService,
    private titleService: Title
  ) {}

  ngOnInit() {
    this.titleService.setTitle("Quackr - Frontpage");
    this.update();
    this.loggedInUserSubscription = this.indexService.getAuthUserSubject().subscribe(
      (userDto: UserDto) => {
        this.loggedInUser = userDto;
      }
    )
  }

  ngOnDestroy() {
    if (this.quacksSubscription != null) {
      this.quacksSubscription.unsubscribe();
    }
    if (this.loggedInUserSubscription != null) {
      this.loggedInUserSubscription.unsubscribe();
    }
  }

  update() {
    if (this.quacksSubscription != null) {
      this.quacksSubscription.unsubscribe();
    }

    this.quacksSubscription = this.quackService.list().subscribe(
      (quacks: QuackDto[]) => {
        this.quacks = quacks;
        this.quacksSubscription = null;
      }
    );
  }

}
