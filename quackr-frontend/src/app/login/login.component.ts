import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { IndexService } from '../services/index.service';
import { LoginAttemptDto, LoginService } from './../services/login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginAttemptDto: LoginAttemptDto = {username: "", password: ""} as LoginAttemptDto;
  canSubmit: boolean = true;
  errorMessages: string[] = [];

  constructor(
    private loginService: LoginService,
    private indexService: IndexService,
    private router: Router,
    private titleService: Title
  ) {}

  ngOnInit() {
    this.titleService.setTitle("Quackr - Login");
  }

  attemptLogin() {
    this.canSubmit = false;
    this.loginService.login(this.loginAttemptDto).finally(() => {
      this.canSubmit = true;
    }).subscribe(result => {
      if (result.status == 200) {
        this.indexService.update();
        this.errorMessages = [];
        this.router.navigate(["/"]);
      } else {
        Observable.throw("Bad status: " + result.status + "\n" + result);
      }
    }, err => {
      if (err.status == 403) {
        let apiError = err.json();

        if(apiError.errors != null) {
          for(let error of apiError.errors) {
            this.errorMessages.push(error);
          }
        }
      } else {
        this.errorMessages.push("Error logging in: " + err);
      }
    })
  }
}
