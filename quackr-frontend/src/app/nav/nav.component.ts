import { IndexService } from './../services/index.service';
import { LoginService } from './../services/login.service';
import { UserDto } from './../services/user.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  loggedInUser: UserDto;

  constructor(
    private indexService: IndexService,
    private loginService: LoginService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.indexService.update();
    this.indexService.getAuthUserSubject().subscribe((userDto: UserDto) => {
      this.loggedInUser = userDto;
    });
  }

  logout() {
    this.loginService.logout().subscribe(result => {
      this.indexService.update();
      this.router.navigate(["/"]);
    })
  }
}
