import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuackCreateComponent } from './quack-create.component';

describe('QuackCreateComponent', () => {
  let component: QuackCreateComponent;
  let fixture: ComponentFixture<QuackCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuackCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuackCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
