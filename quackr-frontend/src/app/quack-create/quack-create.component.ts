import { Router } from '@angular/router';
import { IndexService } from './../services/index.service';
import { QuackEditDto, QuackService, QuackDto } from './../services/quack.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-quack-create',
  templateUrl: './quack-create.component.html',
  styleUrls: ['./quack-create.component.css']
})
export class QuackCreateComponent implements OnInit {
  quackCreate: QuackEditDto = { message: "" } as QuackEditDto;
  quackCreateCanSubmit: boolean = true;

  @Output()
  public onQuackCreated: EventEmitter<QuackDto> = new EventEmitter<QuackDto>();

  constructor(
    private router: Router,
    private quackService: QuackService,
    private indexService: IndexService
  ) {}

  ngOnInit() {
  }

  postNewQuack() {
    this.quackCreateCanSubmit = false;
    this.quackService.create(this.quackCreate).finally(() => {
      this.quackCreateCanSubmit = true;
    }).subscribe(
      result => {
        this.quackCreate = {message: ""} as QuackEditDto;
        this.onQuackCreated.emit(result);
      },
      err => {
        if (err.status == 403) {
          this.router.navigate(["login"]);
        }
        alert("Couldn't post quack!\n" + err);
      }
    )
  }

}
