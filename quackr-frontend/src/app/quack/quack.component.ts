import { LoginService } from './../services/login.service';
import { Subscription } from 'rxjs/Subscription';
import { UserDto } from './../services/user.service';
import { QuackService, QuackDto, QuackEditDto } from './../services/quack.service';
import { IndexService, LoginInfoDto } from './../services/index.service';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-quack',
  templateUrl: './quack.component.html',
  styleUrls: ['./quack.component.css']
})
export class QuackComponent implements OnInit {
  loginInfoDto: LoginInfoDto;
  private subscription: Subscription;

  quackEditDto: QuackEditDto;
  canSubmitEdit: boolean = true;

  @ViewChild("quackEditMessage")
  private quackEditMessageElementRef: ElementRef;

  @Input()
  public quackDto: QuackDto;

  @Output()
  public onRequestDelete: EventEmitter<QuackDto> = new EventEmitter<QuackDto>();

  constructor(
    private indexService: IndexService,
    private quackService: QuackService,
  ) { }

  ngOnInit() {
    this.subscription = this.indexService.getLoginInfoDtoSubject().subscribe((loginInfoDto: LoginInfoDto) => {
      this.loginInfoDto = loginInfoDto;
    });
  }

  showEdit() {
    this.quackEditDto = this.quackService.quackDtoToQuackEditDto(this.quackDto);
    setTimeout(0, () => {
      this.quackEditMessageElementRef.nativeElement.select();
      this.quackEditMessageElementRef.nativeElement.focus();
    });
  }

  hideEdit() {
    this.quackEditDto = null;
  }

  edit() {
    this.canSubmitEdit = false;
    this.quackService.edit(this.quackDto.id, this.quackEditDto).finally(() => {
      this.canSubmitEdit = true;
    }).subscribe((quackDto: QuackDto) => {
      this.quackDto = quackDto;
      this.hideEdit();
    }, err => {
      alert("Couldn't save quack!\n" + err);
    });
  }

  delete() {
    this.onRequestDelete.emit(this.quackDto);
  }

  canEdit() {
    return this.loginInfoDto.authUser != null && this.quackDto.owner.id == this.loginInfoDto.authUser.id;
  }

  canDelete() {
    return this.loginInfoDto.authUser != null && this.quackDto.owner.id == this.loginInfoDto.authUser.id || this.loginInfoDto.authRoles.includes("ADMIN");
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  isAQuackByLoggedInUser() {
    return this.loginInfoDto.authUser != null && this.quackDto.owner.id == this.loginInfoDto.authUser.id;
  }

}
