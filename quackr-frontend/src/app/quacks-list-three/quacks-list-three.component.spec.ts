import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuacksListThreeComponent } from './quacks-list-three.component';

describe('QuacksListThreeComponent', () => {
  let component: QuacksListThreeComponent;
  let fixture: ComponentFixture<QuacksListThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuacksListThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuacksListThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
