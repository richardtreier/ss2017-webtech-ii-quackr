import { UserDto } from './../services/user.service';
import { QuackDto, QuackService } from './../services/quack.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-quacks-list-three',
  templateUrl: './quacks-list-three.component.html',
  styleUrls: ['./quacks-list-three.component.css']
})
export class QuacksListThreeComponent implements OnInit {
  public itemsPerRow: number = 3;

  @Input()
  public quacks: QuackDto[];

  constructor(
    private quackService: QuackService,
  ) { }

  ngOnInit() {
  }

  getNumQuacks(): number {
    return ((this.quacks == null) ? 0 : this.quacks.length);
  }

  getRows(): number[] {
    return Array.from(
      Array(Math.ceil(this.getNumQuacks() / this.itemsPerRow)).keys()
    );
  }

  delete(quackDto: QuackDto) {
    this.quackService.delete(quackDto.id).subscribe(result => {
      this.quacks = this.quacks.filter(quack => quack.id != quackDto.id);
    }, err => {
      alert("Couldn't delete Quack!\n" + err);
    })
  }

}
