import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { IndexService } from '../services/index.service';
import { UserRegistrationDto, UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userRegistrationDto: UserRegistrationDto = { username: "", password: "" } as UserRegistrationDto;

  errorMessages: string[] = [];
  canSubmit: boolean = true;

  constructor(
    private userService: UserService,
    private indexService: IndexService,
    private router: Router,
    private titleService: Title
  ) {}

  ngOnInit() {
    this.titleService.setTitle("Quackr - Sign Up");
  }

  register() {
    this.canSubmit = false;
    this.userService.registerAndLogin(this.userRegistrationDto).finally(() => {
      this.canSubmit = true;
    }).subscribe(result => {
      this.indexService.update();
      this.errorMessages = [];
      this.router.navigate(["/"]);
    }, err => {
      if (err.status == 403) {
        let apiError = err.json();

        if (apiError.errors != null) {
          for (let error of apiError.errors) {
            this.errorMessages.push(error);
          }
        }
      } else {
        this.errorMessages.push("Error submitting registration: " + err);
      }
    })
  }
}
