import { RequestOptions, Headers } from '@angular/http';
import { environment } from '../../environments/environment';

export const quackrHttpApiEndpoint = environment.httpApiEndpoint;
export const quackrHttpHeaders = new Headers({
    'Content-Type': 'application/json'
});
export const quackrHttpRequestOptions = new RequestOptions({
    headers: quackrHttpHeaders,
    withCredentials: environment.httpUseCredentials
});


export function quackrApiResourceUrl(path: string) {
    return quackrHttpApiEndpoint + "/" + path;
}