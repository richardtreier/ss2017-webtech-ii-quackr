import { quackrHttpApiEndpoint, quackrHttpRequestOptions } from './http-config';
import { BehaviorSubject, Subject } from 'rxjs/Rx';
import { LoginInfoDto } from './index.service';
import { UserDto } from './user.service';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

export interface LoginInfoDto {
  authUser: UserDto;
  authRoles: string[];
  authPermissions: string[];
}

@Injectable()
export class IndexService {
  private apiResourceUrl = quackrHttpApiEndpoint;

  private loginInfoDtoSubject: Subject<LoginInfoDto> = new BehaviorSubject<LoginInfoDto>({
    authUser: null,
    authRoles: [],
    authPermissions: []
  } as LoginInfoDto);
  private authUserSubject: Subject<UserDto>;
  private authRolesSubject: Subject<string[]>;
  private authPermissionsSubject: Subject<string[]>;

  constructor(
    private http: Http,
  ) {
    // Feed the main Subject into the Child Subjects
    this.getLoginInfoDtoSubject().subscribe((loginInfoDto: LoginInfoDto) => {
      if(this.authUserSubject == null) {
        this.authUserSubject = new BehaviorSubject<UserDto>(loginInfoDto.authUser);
      } else {
        this.authUserSubject.next(loginInfoDto.authUser);
      }

      if(this.authRolesSubject == null) {
        this.authRolesSubject = new BehaviorSubject<string[]>(loginInfoDto.authRoles);
      } else {
        this.authRolesSubject.next(loginInfoDto.authRoles);
      }

      if(this.authPermissionsSubject == null) {
        this.authPermissionsSubject = new BehaviorSubject<string[]>(loginInfoDto.authPermissions);
      } else {
        this.authPermissionsSubject.next(loginInfoDto.authPermissions);
      }
    });
  }

  public getLoginInfoDtoSubject(): Subject<LoginInfoDto> {
    return this.loginInfoDtoSubject;
  }

  public getAuthUserSubject(): Subject<UserDto> {
    return this.authUserSubject;
  }

  public getAuthRolesSubject(): Subject<string[]> {
    return this.authRolesSubject;
  }

  public getAuthPermissionsSubject(): Subject<string[]> {
    return this.authPermissionsSubject;
  }

  update() {
    this.http.get(this.apiResourceUrl, quackrHttpRequestOptions).map(result => result.json() as LoginInfoDto).subscribe(
      (loginInfoDto: LoginInfoDto) => {
        this.loginInfoDtoSubject.next(loginInfoDto);
      }
    )
  }

}
