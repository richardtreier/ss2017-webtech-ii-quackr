import { Observable } from 'rxjs/Observable';
import { LoginAttemptDto } from './login.service';
import { Http } from '@angular/http';
import { quackrApiResourceUrl, quackrHttpRequestOptions } from './http-config';
import { Injectable } from '@angular/core';

export interface LoginAttemptDto {
  username: string;
  password: string;
}

@Injectable()
export class LoginService {
  private apiLoginUrl = quackrApiResourceUrl("login");
  private apiLogoutUrl = quackrApiResourceUrl("logout");

  constructor(
    private http: Http
  ) { }

  public login(loginAttemptDto: LoginAttemptDto): Observable<any> {
    return this.http.post(this.apiLoginUrl, loginAttemptDto, quackrHttpRequestOptions);
  }

  public logout(): Observable<any> {
    return this.http.post(this.apiLogoutUrl, null, quackrHttpRequestOptions);
  }
}
