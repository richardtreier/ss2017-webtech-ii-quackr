import { UserDto } from './user.service';
import { quackrApiResourceUrl, quackrHttpRequestOptions } from './http-config';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map'

export interface QuackDto {
  id: number;
  message: string;
  owner: UserDto;
  created: Date;
}

export interface QuackEditDto {
  message: string;
}

@Injectable()
export class QuackService {
  private resourceUrl = quackrApiResourceUrl("quacks");

  constructor(
    private http: Http,
  ) { }

  public quackDtoToQuackEditDto(quackDto: QuackEditDto) {
    let quackEditDto = {} as QuackEditDto;

    Object.assign(quackEditDto, quackDto);
    delete quackEditDto["id"];
    delete quackEditDto["owner"];
    delete quackEditDto["created"];

    return quackEditDto;
  }

  public list(): Observable<QuackDto[]> {
    return this.http.get(this.resourceUrl, quackrHttpRequestOptions).map(response => response.json() as QuackDto[]);
  }

  public detail(id: number): Observable<QuackDto> {
    return this.http.get(this.resourceUrl + "/" + id, quackrHttpRequestOptions).map(response => response.json() as QuackDto);
  }

  public create(quackEditDto: QuackEditDto): Observable<QuackDto> {
    return this.http.post(this.resourceUrl, quackEditDto, quackrHttpRequestOptions).map(response => response.json() as QuackDto);
  }

  public edit(id: number, quackEditDto: QuackEditDto): Observable<QuackDto> {
    return this.http.put(this.resourceUrl + "/" + id, quackEditDto, quackrHttpRequestOptions).map(response => response.json() as QuackDto);
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(this.resourceUrl + "/" + id, quackrHttpRequestOptions);
  }

}
