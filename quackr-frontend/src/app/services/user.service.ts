import { QuackDto } from './quack.service';
import { UserRegistrationDto } from './user.service';
import { Observable } from 'rxjs/Observable';
import { quackrApiResourceUrl, quackrHttpRequestOptions } from './http-config';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'

export interface UserDto {
  id: number;
  username: string;
}
export interface UserDetailDto extends UserDto {
  quacks: QuackDto[];
}
export interface UserRegistrationDto {
  username: string;
  password: string;
}


@Injectable()
export class UserService {
  private resourceUrl = quackrApiResourceUrl("users");

  constructor(
    private http: Http,
  ) { }

  public list(): Observable<UserDto[]> {
    return this.http.get(this.resourceUrl, quackrHttpRequestOptions).map(response => response.json() as UserDto[]);
  }

  public detail(username: string): Observable<UserDetailDto> {
    return this.http.get(this.resourceUrl + "/" + username, quackrHttpRequestOptions).map(response => response.json() as UserDetailDto);
  }

  public registerAndLogin(userRegistrationDto: UserRegistrationDto): Observable<UserDto> {
    return this.http.post(this.resourceUrl, userRegistrationDto, quackrHttpRequestOptions).map(response => response.json() as UserDto);
  }


}
