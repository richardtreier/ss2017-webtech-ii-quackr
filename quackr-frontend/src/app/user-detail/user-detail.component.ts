import { Subscription } from 'rxjs/Rx';
import { IndexService } from '../services/index.service';
import { Title } from '@angular/platform-browser';
import { UserDetailDto, UserDto, UserService } from './../services/user.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  userDetailDto: UserDetailDto;
  username: string;
  loggedInUser: UserDto;
  loggedInUserSubscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private titleService: Title,
    private indexService: IndexService,
  ) { }

  ngOnInit() {
    this.titleService.setTitle("Quackr");

    this.route.params.subscribe((p: Params) => {
      this.username = p["username"];
      this.update();
    });

    this.indexService.getAuthUserSubject().subscribe((loggedInUser: UserDto) => {
      this.loggedInUser = loggedInUser;
    });
  }

  update() {
    this.userService.detail(this.username).subscribe((userDetailDto: UserDetailDto) => {
      this.userDetailDto = userDetailDto;

      this.titleService.setTitle("Quackr - " + this.userDetailDto.username);
    }, err => {
      if(err.status == 404) {
        this.router.navigate(["404"]);
      } else {
        alert("Couldn't fetch quacks!\n" + err);
      }
    });
  }

  ngOnDestroy() {
    if(this.loggedInUserSubscription != null) {
      this.loggedInUserSubscription.unsubscribe();
    }
  }

  isLoggedInUsersProfilePage(): boolean {
    return this.userDetailDto != null && this.loggedInUser != null && this.userDetailDto.id == this.loggedInUser.id;
  }

}
