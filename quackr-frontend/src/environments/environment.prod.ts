export const environment = {
  production: true,
  httpUseCredentials: false,
  httpApiEndpoint: "http://localhost:8080/api/v1",
}
