package edu.udo.webtech2.ss17.quackr.web;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import org.springframework.boot.SpringApplication;

@SpringBootApplication(scanBasePackages = { "edu.udo.webtech2.ss17.quackr.dao.config",
		"edu.udo.webtech2.ss17.quackr.core.config", "edu.udo.webtech2.ss17.quackr.web.config", })
public class QuackrSpringBootApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
		return application.sources(QuackrSpringBootApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(QuackrSpringBootApplication.class, args);
	}
}