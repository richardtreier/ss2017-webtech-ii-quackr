package edu.udo.webtech2.ss17.quackr.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Erlaubt Zugriffe von localhost:4200 auf localhost:8080, nur für dev
 * @author Richard
 *
 */
@Profile("local-dev")
@Configuration
public class LocalDevCorsFilter {

	@Bean
	@Autowired
	public FilterRegistrationBean corsFilterRegistrationBean() {
		final FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(corsFilter());
		// Set Highest precedence: CORS filter has to be processed before any
		// other filter, particularly any security filter
		registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return registration;
	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);

		// This is really insecure. In the production environment, the frontend
		// will be served from the same domain, so this won't be necessary. For
		// development it's required to allow localhost:4200 to access
		// localhost:8080 for example.
		config.addAllowedOrigin("*");

		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("DELETE");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}
}
