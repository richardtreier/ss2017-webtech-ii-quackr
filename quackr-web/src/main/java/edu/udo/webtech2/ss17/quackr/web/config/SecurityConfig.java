package edu.udo.webtech2.ss17.quackr.web.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;

import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.authc.AnonymousFilter;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.apache.shiro.web.filter.authc.UserFilter;
import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import edu.udo.webtech2.ss17.quackr.core.security.UserSecurityRealm;

/**
 * Configure Spring w/ Apache Shiro
 * 
 * @author Richard
 *
 */
@Configuration
public class SecurityConfig {

	@Bean(name = "shiroFilter")
	public AbstractShiroFilter shiroFilter() throws Exception {
		ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();

		Map<String, String> filterChainDefinitionMapping = new HashMap<>();
		// filterChainDefinitionMapping.put("/api/health",
		// "authc,roles[guest],ssl[8443]");
		filterChainDefinitionMapping.put("/asdasdasdasdasdasdasd/login", "authc");
		filterChainDefinitionMapping.put("/asdasdasdasdasdasdasd/logout", "logout");
		shiroFilter.setFilterChainDefinitionMap(filterChainDefinitionMapping);

		shiroFilter.setSecurityManager(securityManager());
		shiroFilter.setLoginUrl("/login");

		Map<String, Filter> filters = new HashMap<>();
		filters.put("anon", new AnonymousFilter());
		filters.put("authc", new FormAuthenticationFilter());
		LogoutFilter logoutFilter = new LogoutFilter();
		logoutFilter.setRedirectUrl("/asdasdasdasdasdasdasd/login?logout");
		filters.put("logout", logoutFilter);
		filters.put("roles", new RolesAuthorizationFilter());
		filters.put("user", new UserFilter());
		shiroFilter.setFilters(filters);

		return (AbstractShiroFilter) shiroFilter.getObject();
	}

	@Bean
	public PasswordService passwordService() {
		return new org.apache.shiro.authc.credential.DefaultPasswordService();
	}

	@Bean
	public CredentialsMatcher passwordMatcher() {
		return new org.apache.shiro.authc.credential.PasswordMatcher();
	}

	@Bean
	public UserSecurityRealm userSecurityRealm() {
		UserSecurityRealm userSecurityRealm = new UserSecurityRealm();
		userSecurityRealm.setCredentialsMatcher(passwordMatcher());
		userSecurityRealm.init();

		return userSecurityRealm;
	}

	@Bean(name = "securityManager")
	public WebSecurityManager securityManager() {
		DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
		securityManager.setRealm(userSecurityRealm());
		securityManager.setCacheManager(memoryConstrainedCacheManager());
		return securityManager;
	}

	@Bean
	MemoryConstrainedCacheManager memoryConstrainedCacheManager() {
		MemoryConstrainedCacheManager memoryConstrainedCacheManager = new MemoryConstrainedCacheManager();
		return memoryConstrainedCacheManager;
	}

	@Bean
	public MethodInvokingFactoryBean methodInvokingFactoryBean() {
		MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
		methodInvokingFactoryBean.setStaticMethod("org.apache.shiro.SecurityUtils.setSecurityManager");
		methodInvokingFactoryBean.setArguments(new Object[] { securityManager() });
		return methodInvokingFactoryBean;
	}

	@Bean
	@DependsOn(value = "lifecycleBeanPostProcessor")
	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
		DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
		defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
		return defaultAdvisorAutoProxyCreator;
	}

	@Bean
	public static LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
		return new LifecycleBeanPostProcessor();
	}

	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor() {
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager());
		return authorizationAttributeSourceAdvisor;
	}

}