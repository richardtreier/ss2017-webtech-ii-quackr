package edu.udo.webtech2.ss17.quackr.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import edu.udo.webtech2.ss17.quackr.core.services.ResourceDoesNotExistException;

@ControllerAdvice
public class ErrorControllerAdvice extends ResponseEntityExceptionHandler {

	/**
	 * Some Long -> Entity resolution didnt work... 404
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ edu.udo.webtech2.ss17.quackr.core.services.ResourceDoesNotExistException.class })
	public ResponseEntity<Object> handleResourceDoesNotExistException(
			final edu.udo.webtech2.ss17.quackr.core.services.ResourceDoesNotExistException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(),
				"You can't play soccer without a ball, m8.");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}
	/**
	 * User Already Exists
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ edu.udo.webtech2.ss17.quackr.core.services.UserAlreadyExistsException.class })
	public ResponseEntity<Object> handleUserAlreadyExistsException(
			final edu.udo.webtech2.ss17.quackr.core.services.UserAlreadyExistsException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final ApiError apiError = new ApiError(HttpStatus.CONFLICT, ex.getLocalizedMessage(),
				"This user already exists!");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Apache Shiro Authorization Exception - 403
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ org.apache.shiro.authz.AuthorizationException.class })
	public ResponseEntity<Object> handleShiroUnauthenticatedException(
			final org.apache.shiro.authz.AuthorizationException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(), "Access Denied.");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Apache Shiro Authentication Exception - 403
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ org.apache.shiro.authc.UnknownAccountException.class })
	public ResponseEntity<Object> handleShiroUnknownAccountException(
			final org.apache.shiro.authc.UnknownAccountException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(), "Bad Username");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Apache Shiro Authentication Exception - 403
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ org.apache.shiro.authc.LockedAccountException.class })
	public ResponseEntity<Object> handleShiroLockedAccountException(
			final org.apache.shiro.authc.LockedAccountException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(), "Account is locked.");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Apache Shiro Authentication Exception - 403
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ org.apache.shiro.authc.ExcessiveAttemptsException.class })
	public ResponseEntity<Object> handleShiroExcessiveAttemptsException(
			final org.apache.shiro.authc.ExcessiveAttemptsException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(),
				"Too many login attempts!");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Apache Shiro Authentication Exception - 403
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ org.apache.shiro.authc.IncorrectCredentialsException.class })
	public ResponseEntity<Object> handleShiroIncorrectCredentialsException(
			final org.apache.shiro.authc.IncorrectCredentialsException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(),
				"Bad Username / Password.");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Apache Shiro Authentication Exception - 403
	 *
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ org.apache.shiro.authc.AuthenticationException.class })
	public ResponseEntity<Object> handleShiroAuthenticationException(
			final org.apache.shiro.authc.AuthenticationException ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final ApiError apiError = new ApiError(HttpStatus.FORBIDDEN, ex.getLocalizedMessage(),
				"Authentication Failed!");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Bad Request: 400
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final List<String> errors = new ArrayList<String>();
		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}

	/**
	 * Bad Request: 400
	 */
	@Override
	protected ResponseEntity<Object> handleBindException(final BindException ex, final HttpHeaders headers,
			final HttpStatus status, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final List<String> errors = new ArrayList<String>();
		for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}
		for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}

	/**
	 * Bad Request: 400
	 */
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers,
			final HttpStatus status, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final String error = ex.getValue() + " value for " + ex.getPropertyName() + " should be of type "
				+ ex.getRequiredType();

		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Bad Request: 400
	 */
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestPart(final MissingServletRequestPartException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final String error = ex.getRequestPartName() + " part is missing";
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Bad Request: 400
	 */
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			final MissingServletRequestParameterException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final String error = ex.getParameterName() + " parameter is missing";
		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Bad Request: 400
	 */
	@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(final MethodArgumentTypeMismatchException ex,
			final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();

		final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}
	// /**
	// * Bad Request: 400
	// */
	// @ExceptionHandler({ javax.validation.ConstraintViolationException.class
	// })
	// public ResponseEntity<Object> handleConstraintViolation(final
	// javax.validation.ConstraintViolationException ex, final WebRequest
	// request) {
	// logger.info(ex.getClass().getName());
	// //
	// final List<String> errors = new ArrayList<String>();
	// for (final javax.validation.ConstraintViolation<?> violation :
	// ex.getConstraintViolations()) {
	// errors.add(violation.getRootBeanClass().getName() + " " +
	// violation.getPropertyPath() + ": " + violation.getMessage());
	// }
	//
	// final ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST,
	// ex.getLocalizedMessage(), errors);
	// return new ResponseEntity<Object>(apiError, new HttpHeaders(),
	// apiError.getStatus());
	// }

	/**
	 * Resource Not Found: 404
	 */
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(final NoHandlerFoundException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

		final ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Method Not Allowed: 405
	 */
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			final HttpRequestMethodNotSupportedException ex, final HttpHeaders headers, final HttpStatus status,
			final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final StringBuilder builder = new StringBuilder();
		builder.append(ex.getMethod());
		builder.append(" method is not supported for this request. Supported methods are ");
		ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));

		final ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, ex.getLocalizedMessage(),
				builder.toString());
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * Media Type Not SUpported: 415
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(final HttpMediaTypeNotSupportedException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		logger.info(ex.getClass().getName());
		//
		final StringBuilder builder = new StringBuilder();
		builder.append(ex.getContentType());
		builder.append(" media type is not supported. Supported media types are ");
		ex.getSupportedMediaTypes().forEach(t -> builder.append(t + " "));

		final ApiError apiError = new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, ex.getLocalizedMessage(),
				builder.substring(0, builder.length() - 2));
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

	/**
	 * All unhandled Exceptions: 500
	 */
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
		logger.info(ex.getClass().getName());
		logger.error("error", ex);
		//
		final ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(),
				"error occurred");
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}

}