package edu.udo.webtech2.ss17.quackr.web.controller;

import org.apache.shiro.authc.credential.PasswordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.udo.webtech2.ss17.quackr.core.security.LoggedInUserService;

@RestController
@RequestMapping("api/v1")
public class IndexController {
	private static final Logger log = LoggerFactory.getLogger(IndexController.class);
	
	@Autowired
	private transient LoggedInUserService loggedInUserService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> index() {		
		return ResponseEntity.ok(loggedInUserService.getLoginStatus());
	}
}
