package edu.udo.webtech2.ss17.quackr.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.udo.webtech2.ss17.quackr.core.security.LoginAttemptDto;
import edu.udo.webtech2.ss17.quackr.core.security.LoginService;

@RestController
@RequestMapping("/api/v1")
public class LoginController {
	@Autowired
	private transient LoginService loginService;
	
	@PostMapping(value = "/login")
	public ResponseEntity<?> login(@RequestBody LoginAttemptDto loginAttemptDto) {
		loginService.login(loginAttemptDto);
		return ResponseEntity.ok().build();
	}
	@PostMapping(value = "/logout")
	public ResponseEntity<?> logout() {
		loginService.logout();
		return ResponseEntity.ok().build();
	}
}
