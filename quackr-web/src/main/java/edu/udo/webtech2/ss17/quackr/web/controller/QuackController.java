package edu.udo.webtech2.ss17.quackr.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.udo.webtech2.ss17.quackr.core.dto.QuackDto;
import edu.udo.webtech2.ss17.quackr.core.dto.QuackEditDto;
import edu.udo.webtech2.ss17.quackr.core.services.QuackService;

@RestController
@RequestMapping("api/v1/quacks")
public class QuackController {
	@Autowired
	private transient QuackService quackService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> detail() {
		return ResponseEntity.ok(quackService.findAllOrderByCreatedDesc());
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> detail(@PathVariable("id") Long id) {
		return ResponseEntity.ok(quackService.findOne(id));
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> create(@RequestBody QuackEditDto quackEditDto) {
		QuackDto quackDto = quackService.create(quackEditDto);

		return ResponseEntity
				.created(ControllerLinkBuilder
						.linkTo(ControllerLinkBuilder.methodOn(QuackController.class).detail(quackDto.getId())).toUri())
				.body(quackDto);
	}

	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> edit(@PathVariable("id") Long id, @RequestBody QuackEditDto quackEditDto) {
		QuackDto quackDto = quackService.edit(id, quackEditDto);

		return ResponseEntity.ok(quackDto);
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> delete(@PathVariable("id") Long id) {
		quackService.delete(id);

		return ResponseEntity.ok().build();
	}
}
