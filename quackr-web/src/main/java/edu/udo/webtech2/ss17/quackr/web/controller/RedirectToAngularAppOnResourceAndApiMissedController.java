package edu.udo.webtech2.ss17.quackr.web.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RedirectToAngularAppOnResourceAndApiMissedController implements ErrorController {

    @RequestMapping("/error")
    public String index() {
        return "forward:/";
    }

    @Override
    public String getErrorPath() {
        return "forward:/";
    }
}