package edu.udo.webtech2.ss17.quackr.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.udo.webtech2.ss17.quackr.core.dto.UserDto;
import edu.udo.webtech2.ss17.quackr.core.dto.UserRegistrationDto;
import edu.udo.webtech2.ss17.quackr.core.services.UserService;

@RestController
@RequestMapping("api/v1/users")
public class UserController {
	@Autowired
	private transient UserService userService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> list() {
		return ResponseEntity.ok(userService.findAllOrderByUsernameAsc());
	}

	@GetMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> detail(@PathVariable("username") String username) {
		return ResponseEntity.ok(userService.findOneByUsernameDetail(username));
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> signup(@RequestBody UserRegistrationDto userRegistrationDto) {
		UserDto userDto = userService.createUserAndLogin(userRegistrationDto);

		return ResponseEntity.created(ControllerLinkBuilder
				.linkTo(ControllerLinkBuilder.methodOn(UserController.class).detail(userDto.getUsername())).toUri())
				.body(userDto);
	}
}
